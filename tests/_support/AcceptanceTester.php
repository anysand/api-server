<?php
namespace App\Tests;

use App\Security\Domain\User;
use Behat\Gherkin\Node\PyStringNode;
use Codeception\Util\Fixtures;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Ramsey\Uuid\Uuid;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    private mixed $requestPayload;

    /**
     * @When I add :arg1 header equal to :arg2
     */
    public function iAddHeaderEqualTo($arg1, $arg2)
    {
        $this->haveHttpHeader($arg1, $arg2);
    }


    /**
     * @When I authorized
     */
    public function iAuthorized()
    {
        /** @var JWTTokenManagerInterface $tokenManager */
        $tokenManager = $this->grabService(JWTTokenManagerInterface::class);
        $userFixture = Fixtures::get('user')['test'];
        $token = $tokenManager->create(new User($userFixture['id'], $userFixture['email_value'], $userFixture['password_value']));
        $this->haveHttpHeader('Authorization', 'Bearer '.$token);
    }

    /**
     * @When I send a :arg1 request to :arg2
     */
    public function iSendARequestTo($arg1, $arg2)
    {
        $this->send($arg1, $arg2);
    }

    /**
     * @Then the response status code should be :code
     */
    public function theResponseStatusCodeShouldBe($code)
    {
        $this->seeResponseCodeIs($code);
    }

    /**
     * @Then the response should be in JSON
     */
    public function theResponseShouldBeInJSON()
    {
        $this->seeResponseIsJson();
    }

    /**
     * @Then the JSON node :arg1 should be exist
     */
    public function theJSONNodeShouldBeExist($arg1)
    {
        $this->seeResponseJsonMatchesJsonPath($arg1);
    }

    /**
     * @Then the JSON node :arg1 should be equal to :arg2
     */
    public function theJSONNodeShouldBeEqualTo($arg1, $arg2)
    {
        $this->seeResponseContainsJson([$arg1 => $arg2]);
    }

    /**
     * @Then the JSON response should be matches path :arg1
     */
    public function theJSONResponseShouldBeMatchesPath($arg1)
    {
        $this->seeResponseJsonMatchesJsonPath($arg1);
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE) ([^"]*)" with payload (.*)$/
     */
    public function iRequest($httpMethod, $resource, PyStringNode $payload)
    {
        $params = json_decode($payload->getRaw(), true);
        $this->send($httpMethod, $resource, $params ?: []);
    }
}
