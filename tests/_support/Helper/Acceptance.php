<?php

namespace App\Tests\Helper;

use Codeception\Module\Db;
use Codeception\TestInterface;
use Codeception\Util\Fixtures;

class Acceptance extends Db
{
    public function _before(TestInterface $test)
    {
        parent::_before($test);
        $dbh = $this->_getDriver()->getDbh();
        $dbh->exec('TRUNCATE TABLE "user";');
        $dbh->exec('TRUNCATE TABLE merchant;');
        $dbh->exec('TRUNCATE TABLE product;');

        foreach (Fixtures::get('user') as $item) {
            $this->haveInDatabase('user', $item);
        }
        foreach (Fixtures::get('merchant') as $item) {
            $this->haveInDatabase('merchant', $item);
        }
        foreach (Fixtures::get('product') as $item) {
            $this->haveInDatabase('product', $item);
        }
    }
}
