<?php

return [
    'test' => [
        'id' => '00b93ca9-2031-4e06-be34-05a339bc9907',
        'email_value' => 'user@test.com',
        'password_value' => '$2y$13$q/I9I2ZfdKlUbRCYJIrmJurXCljlrIf7b.G9jlHVUtRWcmHVTb8Cm',
    ]
];
