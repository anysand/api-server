<?php

use Codeception\Util\Fixtures;

const FIXTURES_DIR = __DIR__ . '/../_fixtures/';

Fixtures::add('user', require(FIXTURES_DIR . 'user.php'));
Fixtures::add('merchant', require(FIXTURES_DIR . 'merchant.php'));
Fixtures::add('product', require(FIXTURES_DIR . 'product.php'));
