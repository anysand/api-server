Feature: security
  In order to prove that the user can't get data without auth

  Scenario: It receives a response 401
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/users/00b93ca9-2031-4e06-be34-05a339bc9907"

    Then the response status code should be "401"
    And the response should be in JSON
    And the JSON node "code" should be equal to "401"
    And the JSON node "message" should be equal to "JWT Token not found"


  Scenario: Success Login
    And I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I request "POST /api/login" with payload
     """
      {
        "username": "user@test.com",
        "password": "test"
       }
     """

    Then the response status code should be "200"
     And the JSON node "token" should be exist



  Scenario: It receives a response 200
    When I authorized
    And I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/users/00b93ca9-2031-4e06-be34-05a339bc9907"

    Then the response status code should be "200"
