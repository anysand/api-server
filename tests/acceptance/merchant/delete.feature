Feature: merchants/delete
  In order to prove that merchants can be created correctly

  Scenario: try delete merchant
    When I authorized
     And I add "Content-Type" header equal to "application/json"
     And I add "Accept" header equal to "application/json"
     And I request "DELETE /api/v1/merchants/00b93ca9-2031-4e06-be34-05a339bc9907" with payload
     """
     """

    Then the response status code should be "204"


