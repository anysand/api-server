Feature: merchants/add
  In order to prove that merchants can be created correctly

  Scenario: try add merchant
    When I authorized
     And I add "Content-Type" header equal to "application/json"
     And I add "Accept" header equal to "application/json"
     And I request "POST /api/v1/merchants" with payload
     """
      {
        "name": "test name"
      }
     """

    Then the response status code should be "201"
     And the response should be in JSON
     And the JSON node "uuid" should be exist


