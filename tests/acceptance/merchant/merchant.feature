Feature: merchants/merchant
  In order to prove that the merchant get correctly by id
  As a merchant
  I want to have a scenario when I get merchant

  Scenario: It receives a success merchant response
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I authorized
    And I send a "GET" request to "/api/v1/merchants/00b93ca9-2031-4e06-be34-05a339bc9907"

    Then the response status code should be "200"
    And the response should be in JSON
    And the JSON node "id" should be equal to "00b93ca9-2031-4e06-be34-05a339bc9907"
    And the JSON node "name" should be equal to "some name"
