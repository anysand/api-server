Feature: products/list
  In order to prove that products list get correctly
  As a product I want receive list of products
  I need to ...

  Scenario: try retrieve list of products
    When I authorized
     And I add "Content-Type" header equal to "application/json"
     And I add "Accept" header equal to "application/json"
     And I send a "GET" request to "/api/v1/products/?filter={}&range=[0,10]"

    Then the response status code should be "200"
     And the response should be in JSON
     And the JSON response should be matches path "$..id"
     And the JSON response should be matches path "$..name"


