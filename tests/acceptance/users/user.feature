Feature: users/user
  In order to prove that the user get correctly by id
  As a user
  I want to have a scenario when I get user

  Scenario: It receives a success user response
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I authorized
    And I send a "GET" request to "/api/v1/users/00b93ca9-2031-4e06-be34-05a339bc9907"

    Then the response status code should be "200"
    And the response should be in JSON
    And the JSON node "id" should be equal to "00b93ca9-2031-4e06-be34-05a339bc9907"
    And the JSON node "email" should be exist
