Feature: users/add
  In order to prove that users can be created correctly

  Scenario: try add user
    When I authorized
     And I add "Content-Type" header equal to "application/json"
     And I add "Accept" header equal to "application/json"
     And I request "POST /api/v1/users" with payload
     """
      {
        "email": "test@email.com",
        "password": "password"
       }
     """

    Then the response status code should be "201"
     And the response should be in JSON
     And the JSON node "uuid" should be exist


