<?php

namespace App\Common\Domain\View;

interface ListViewInterface
{
    public function getElements();

    public function getTotalCount(): int;

    public function getStartRange(): int;

    public function getEndRange(): int;
}
