<?php

declare(strict_types=1);

namespace App\Common\Domain\ValueObject;

use App\Common\Domain\Exception\InvalidUUIDException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AggregateRootId
{
    protected UuidInterface $uuid;

    public function __construct(string $id = null)
    {
        try {
            $this->uuid = $id ? Uuid::fromString($id) : Uuid::uuid4();
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUUIDException();
        }
    }

    public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid->equals($aggregateRootId->uuid);
    }

    public function __toString(): string
    {
        return $this->uuid->__toString();
    }
}
