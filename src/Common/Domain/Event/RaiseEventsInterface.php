<?php

namespace App\Common\Domain\Event;

interface RaiseEventsInterface
{
    /**
     * Return events raised by the entity and clear those.
     */
    public function popEvents(): array;
}
