<?php

namespace App\Common\Domain\Event;

trait RaiseEventsTrait
{
    protected array $events = [];

    public function popEvents(): array
    {
        $events = $this->events;

        $this->events = [];

        return $events;
    }

    protected function raise(Event $event): void
    {
        $this->events[] = $event;
    }
}
