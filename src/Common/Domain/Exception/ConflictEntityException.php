<?php

declare(strict_types=1);

namespace App\Common\Domain\Exception;

use RuntimeException;

class ConflictEntityException extends RuntimeException
{
}
