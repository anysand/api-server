<?php

declare(strict_types=1);

namespace App\Common\Presentation\Http\Rest\Request;

use App\Common\Presentation\Http\Rest\Exception\RequestValidationFailedException;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    public function __construct(ValidatorInterface $validator, SerializerInterface $serializer)
    {
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_subclass_of($argument->getType(), RequestInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): Generator
    {
        if ($request->isMethod(Request::METHOD_GET)) {
            $data = array_map(static function ($param) {
                return json_decode($param, true, 512, JSON_THROW_ON_ERROR);
            }, $request->query->all());

            $data = json_encode($data, JSON_THROW_ON_ERROR);
        } else {
            $data = $request->getContent();
        }
        $customRequest = $this->serializer->deserialize(
            $data,
            $argument->getType(),
            'json',
            [
                AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
            ]
        );

        $errors = $this->validator->validate($customRequest);

        if (count($errors) > 0) {
            throw new RequestValidationFailedException($errors);
        }

        yield $customRequest;
    }
}
