<?php

namespace App\Common\Presentation\Http\Rest\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Throwable;

class ErrorResponse extends JsonResponse
{
    public static function createFromArray(string $message, array $details, int $code = 400): self
    {
        return new self([
            'error' => [
                'message' => $message,
                'details' => $details
            ]
        ], $code);
    }

    public static function createFromException(Throwable $e, int $code = 400): self
    {
        return new self([
            'error' => [
                'message' => $e->getMessage()
            ]
        ], $code);
    }
}
