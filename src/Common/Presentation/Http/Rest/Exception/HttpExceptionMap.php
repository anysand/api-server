<?php

declare(strict_types=1);

namespace App\Common\Presentation\Http\Rest\Exception;

use App\Common\Presentation\Http\Rest\Response\ErrorResponse;
use League\Tactician\Bundle\Middleware\InvalidCommandException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Throwable;

class HttpExceptionMap
{
    private array $exceptionMap;

    public function __construct(array $exceptionMap)
    {
        $this->exceptionMap = $exceptionMap;
    }

    public function resolveExceptionResponse(Throwable $e): JsonResponse
    {
        if ($e instanceof HttpExceptionInterface) {
            return ErrorResponse::createFromException($e, $e->getStatusCode());
        }

        if ($e instanceof RequestValidationFailedException || $e instanceof InvalidCommandException) {
            $errors = array_map(function (ConstraintViolation $constraintViolation) {
                return $constraintViolation->getPropertyPath().' - '.$constraintViolation->getMessage();
            }, iterator_to_array($e->getViolations()));

            return ErrorResponse::createFromArray('Validation Error', $errors, 400);
        }

        foreach ($this->exceptionMap as $exceptionClass => $httpCode) {
            if ($e instanceof $exceptionClass) {
                return ErrorResponse::createFromException($e, $httpCode);
            }
        }

        return ErrorResponse::createFromException($e, 500);
    }
}
