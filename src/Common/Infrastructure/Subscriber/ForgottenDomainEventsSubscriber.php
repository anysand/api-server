<?php

namespace App\Common\Infrastructure\Subscriber;

use Exception;
use RuntimeException;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ForgottenDomainEventsSubscriber implements EventSubscriberInterface
{
    private DomainEventsCollector $domainEventsCollector;

    public function __construct(DomainEventsCollector $domainEventsCollector)
    {
        $this->domainEventsCollector = $domainEventsCollector;
    }

    /**
     * Log if some Domain events were left undispatched.
     * Exceptions thrown on kernel.response are not handled so we can't throw exceptions.
     *
     * @param ResponseEvent $responseEvent
     */
    public function onKernelResponse(ResponseEvent $responseEvent): void
    {
        if (true === $this->domainEventsCollector->hasUndispatchedEvents()) {
            throw new RuntimeException('Some domain events were left undispatched!');
        }
    }

    /**
     * Same check but on console.
     * Here we can throw some exception so the command will fail.
     *
     * @throws Exception
     */
    public function onConsoleTerminate(): void
    {
        if (true === $this->domainEventsCollector->hasUndispatchedEvents()) {
            throw new RuntimeException('Some domain events were left undispatched!');
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
            ConsoleEvents::TERMINATE => 'onConsoleTerminate',
        ];
    }
}