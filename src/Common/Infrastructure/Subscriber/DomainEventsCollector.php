<?php

namespace App\Common\Infrastructure\Subscriber;

use App\Common\Domain\Event\RaiseEventsInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\EventDispatcher\Event;

class DomainEventsCollector implements EventSubscriber
{
    /**
     * @var Event[] Domain events that are queued
     */
    private array $events = [];

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove,
            Events::postRemove,
        ];
    }

    public function postPersist(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function postUpdate(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    /**
     * You need to listen to preRemove if you use soft delete from Doctrine extensions,
     * because it prevents postRemove from being called.
     */
    public function preRemove(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function postRemove(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    /**
     * Returns all collected events and then clear those.
     */
    public function dispatchCollectedEvents(): void
    {
        $events = $this->events;
        $this->events = [];

        foreach ($events as $event) {
            $this->eventDispatcher->dispatch($event);
        }

        // Maybe listeners emitted some new events!
        if ($this->events) {
            $this->dispatchCollectedEvents();
        }
    }

    /**
     * Optional, we will see why it can be useful later.
     */
    public function hasUndispatchedEvents(): bool
    {
        return 0 !== count($this->events);
    }

    private function doCollect(LifecycleEventArgs $lifecycleEvent): void
    {
        $entity = $lifecycleEvent->getEntity();

        if (!$entity instanceof RaiseEventsInterface) {
            return;
        }

        foreach ($entity->popEvents() as $event) {
            // We index by object hash, not to have the same event twice
            $this->events[spl_object_hash($event)] = $event;
        }
    }
}
