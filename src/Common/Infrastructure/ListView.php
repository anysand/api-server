<?php

declare(strict_types=1);

namespace App\Common\Infrastructure;

use Assert\Assertion;

abstract class ListView
{
    private array $elements;

    private int $startRange;

    private int $endRange;

    private int $totalCount;

    public function __construct(array $elements, int $startRange, int $endRange, int $totalCount)
    {
        Assertion::allIsInstanceOf($elements, $this->getElementType());

        $this->elements = $elements;
        $this->startRange = $startRange;
        $this->endRange = $endRange;
        $this->totalCount = $totalCount;
    }

    abstract protected function getElementType(): string;

    public function getElements(): array
    {
        return $this->elements;
    }

    public function getStartRange(): int
    {
        return $this->startRange;
    }

    public function getEndRange(): int
    {
        return $this->endRange;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}
