<?php

namespace App\Security\Infrastructure;

use App\Security\Domain\Repository\UserReadRepositoryInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class Authenticator extends JWTTokenAuthenticator
{
    private UserReadRepositoryInterface $userRepository;

    public function setUserRepository(UserReadRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    protected function loadUser(UserProviderInterface $userProvider, array $payload, $identity)
    {
        return $this->userRepository->findOneByEmail($identity);
    }
}
