<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Repository;

use App\Common\Domain\Exception\EntityNotFoundException;
use App\Security\Domain\Repository\UserReadRepositoryInterface;
use App\Security\Domain\User;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;


class UserReadRepository implements UserReadRepositoryInterface
{
    private const TABLE_NAME = '"user"';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    public function findOneByEmail(string $email): ?User
    {
        $qb = $this->connection->createQueryBuilder();
        $result = $qb->select('*')
            ->from(self::TABLE_NAME)
            ->where('email_value = :email')
            ->setParameter('email', $email)
            ->setMaxResults(1)
            ->fetchAssociative();

        if (empty($result)) {
            return null;
        }

        return new User($result['id'], $result['email_value'], $result['password_value']);
    }
}
