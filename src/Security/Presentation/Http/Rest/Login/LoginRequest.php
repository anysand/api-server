<?php

declare(strict_types=1);

namespace App\Security\Presentation\Http\Rest\Login;

use App\Common\Presentation\Http\Rest\Request\RequestInterface;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class LoginRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank
     * @OA\Property(property="username", type="string")
     */
    private string $username;

    /**
     * @Assert\NotBlank
     */
    private string $password;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
