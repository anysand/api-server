<?php

declare(strict_types=1);

namespace App\Security\Presentation\Http\Rest\Login;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Security\Application\Login\LoginCommand;
use App\Security\Application\Login\LoginResult;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("login", name="login", methods={"POST"})
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns the login data",
 *     @OA\JsonContent()
 * )
 *
 * @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref=@Model(type=LoginRequest::class))
 * )
 * @OA\Tag(name="Security")
 */
class LoginAction implements ActionInterface
{
    public function __invoke(LoginRequest $request, CommandBus $bus): JsonResponse
    {
        $command = new LoginCommand(
            $request->getUsername(),
            $request->getPassword()
        );

        /** @var LoginResult $result */
        $result = $bus->handle($command);

        return new LoginResponse($result);
    }
}
