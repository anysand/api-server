<?php

namespace App\Security\Presentation\Http\Rest\Login;

use App\Security\Application\Login\LoginResult;
use Symfony\Component\HttpFoundation\JsonResponse;


final class LoginResponse extends JsonResponse
{
    public function __construct(LoginResult $result)
    {
        parent::__construct([
            'id' => $result->getUser()->getId(),
            'token' => $result->getToken(),
        ]);
    }
}
