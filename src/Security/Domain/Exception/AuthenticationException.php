<?php

namespace App\Security\Domain\Exception;

use Exception;

class AuthenticationException extends Exception
{
    public function __construct()
    {
        parent::__construct('security.exception.authentication_exception');
    }
}
