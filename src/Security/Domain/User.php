<?php

namespace App\Security\Domain;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private const DEFAULT_ROLES = [
        'ROLE_USER'
    ];
    private string $id;
    private string $username;
    private string $password;
    private array $roles;

    public function __construct(string $id, string $username, string $password, array $roles = [])
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->roles = array_merge(self::DEFAULT_ROLES, $roles);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
