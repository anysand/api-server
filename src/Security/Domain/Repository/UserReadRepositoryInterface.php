<?php

declare(strict_types=1);

namespace App\Security\Domain\Repository;

use App\Security\Domain\User;

interface UserReadRepositoryInterface
{
    public function findOneByEmail(string $email): ?User;
}
