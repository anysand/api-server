<?php

namespace App\Security\Application\Login;

use App\Common\Application\CommandHandlerInterface;
use App\Security\Domain\Exception\AuthenticationException;
use App\Security\Domain\Repository\UserReadRepositoryInterface;
use App\Security\Domain\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class LoginHandler implements CommandHandlerInterface
{
    private UserReadRepositoryInterface $repository;
    private PasswordHasherInterface $hasher;
    private JWTTokenManagerInterface $JWTManager;

    public function __construct(
        UserReadRepositoryInterface    $repository,
        PasswordHasherFactoryInterface $hasherFactory,
        JWTTokenManagerInterface       $JWTManager
    )
    {
        $this->repository = $repository;
        $this->hasher = $hasherFactory->getPasswordHasher(User::class);
        $this->JWTManager = $JWTManager;
    }

    /**
     * @param LoginCommand $command
     *
     * @return LoginResult
     *
     * @throws AuthenticationException
     */
    public function handle(LoginCommand $command): LoginResult
    {
        $user = $this->repository->findOneByEmail($command->getUsername());

        if (!$user) {
            throw new AuthenticationException();
        }

        if (!$this->hasher->verify((string)$user->getPassword(), $command->getPassword())) {
            throw new AuthenticationException();
        }

        $token = $this->JWTManager->create($user);

        return new LoginResult($user, $token);
    }
}
