<?php

declare(strict_types=1);

namespace App\Product\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\Common\Infrastructure\Subscriber\DomainEventsCollector;
use App\Product\Domain\Product;
use App\Product\Domain\Repository\ProductRepositoryInterface;
use App\Product\Domain\ValueObject\ProductId;

class CreateProductHandler implements CommandHandlerInterface
{
    private ProductRepositoryInterface $productRepository;
    private DomainEventsCollector $eventsCollector;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        DomainEventsCollector $eventsCollector)
    {
        $this->productRepository = $productRepository;
        $this->eventsCollector = $eventsCollector;
    }

    public function handle(CreateProductCommand $command): Product
    {
        $product = Product::create(
            new ProductId(),
            $command->getName());

        $this->productRepository->store($product);
        $this->eventsCollector->dispatchCollectedEvents();

        return $product;
    }
}
