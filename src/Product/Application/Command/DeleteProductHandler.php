<?php

declare(strict_types=1);

namespace App\Product\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\Product\Domain\Repository\ProductRepositoryInterface;
use App\Product\Domain\ValueObject\Email;
use App\Product\Domain\ValueObject\ProductId;
use App\Product\Domain\ValueObject\Password;

class DeleteProductHandler implements CommandHandlerInterface
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handle(DeleteProductCommand $command): void
    {
        $product = $this->productRepository->getById(new ProductId($command->getId()));

        $this->productRepository->remove($product);
    }
}
