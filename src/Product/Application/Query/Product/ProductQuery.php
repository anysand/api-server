<?php

declare(strict_types=1);

namespace App\Product\Application\Query\Product;

use Symfony\Component\Validator\Constraints as Assert;

class ProductQuery
{
    /**
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
