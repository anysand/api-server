<?php

declare(strict_types=1);

namespace App\Product\Application\Query\Product;

use App\Common\Application\QueryHandlerInterface;
use App\Product\Domain\Repository\ProductReadRepositoryInterface;
use App\Product\Domain\View\ProductViewInterface;

class ProductHandler implements QueryHandlerInterface
{
    private ProductReadRepositoryInterface $repository;

    public function __construct(ProductReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(ProductQuery $query): ProductViewInterface
    {
        return $this->repository->oneByCriteria(['id' => $query->getId()]);
    }
}
