<?php

declare(strict_types=1);

namespace App\Product\Application\Query\ProductList;

use App\Common\Application\QueryHandlerInterface;
use App\Product\Domain\Repository\ProductReadRepositoryInterface;
use App\Product\Domain\View\ProductListViewInterface;

class ProductListHandler implements QueryHandlerInterface
{
    private ProductReadRepositoryInterface $readRepository;

    public function __construct(ProductReadRepositoryInterface $readRepository)
    {
        $this->readRepository = $readRepository;
    }

    public function handle(ProductListQuery $query): ProductListViewInterface
    {
        return $this->readRepository->findAllByCriteria($query->getFilter(), $query->getRange(), $query->getSort());
    }
}
