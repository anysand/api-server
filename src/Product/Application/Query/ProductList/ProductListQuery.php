<?php

declare(strict_types=1);

namespace App\Product\Application\Query\ProductList;

class ProductListQuery
{
    private array $filter;
    private array $range;
    private array $sort;

    public function __construct(array $filter, array $range, array $sort)
    {
        $this->filter = $filter;
        $this->range = $range;
        $this->sort = $sort;
    }

    /**
     * @return array
     */
    public function getFilter(): array
    {
        return $this->filter;
    }

    /**
     * @return array
     */
    public function getRange(): array
    {
        return $this->range;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }
}
