<?php

declare(strict_types=1);

namespace App\Product\Presentation\Cli\Command;

use App\Product\Application\Command\CreateProductCommand as AppCreateProductCommand;
use Exception;
use League\Tactician\CommandBus;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateProductCommand extends Command
{
    protected static $defaultName = 'app:product:create';

    private ValidatorInterface $validator;
    private CommandBus $bus;

    public function __construct(ValidatorInterface $validator, CommandBus $bus)
    {
        parent::__construct();
        $this->validator = $validator;
        $this->bus = $bus;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Create new product')
            ->setHelp(
                'This command create new product.'
            );
    }

    /**
     * Executes the current command.
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $io = new SymfonyStyle($input, $output);

        $nameQuestion = $this->createEmailQuestion();
        $name = $io->askQuestion($nameQuestion);

        $command = new AppCreateProductCommand(
            $name
        );

        $this->bus->handle($command);

        return Command::SUCCESS;
    }

    /**
     * Create the name question to ask the product for the name.
     */
    private function createEmailQuestion(): Question
    {
        $passwordQuestion = new Question('Type in product name');

        return $passwordQuestion
            ->setValidator(function ($value) {
                $errors = $this->validator->validatePropertyValue(AppCreateProductCommand::class, 'name', $value);

                if (count($errors) > 0) {
                    throw new RuntimeException($errors->get(0)->getMessage());
                }

                return $value;
            })
            ->setMaxAttempts(20);
    }
}
