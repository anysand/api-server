<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Product\Application\Command\CreateProductCommand;
use App\Product\Domain\Product;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Product")
 *
 * @Route("/products", methods={"POST"})
 *
 * @OA\Response(
 *     response=201,
 *     description="Task created successfully"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 * @OA\Response(
 *     response=409,
 *     description="Conflict"
 * )
 */
class ProductCreateAction implements ActionInterface
{
    public function __invoke(ProductCreateRequest $request, CommandBus $bus): JsonResponse
    {
        $command = new CreateProductCommand($request->getName());

        /** @var Product $product */
        $product = $bus->handle($command);

        return new JsonResponse([
            'uuid' => (string) $product->getId()
        ], Response::HTTP_CREATED);
    }
}
