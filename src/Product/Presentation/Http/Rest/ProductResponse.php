<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Product\Domain\View\ProductViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductResponse extends JsonResponse
{
    public function __construct(ProductViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
        );
    }

    private function makePayload(ProductViewInterface $view): array
    {
        return [
            'id' => $view->getId(),
            'name' => $view->getName(),
        ];
    }
}
