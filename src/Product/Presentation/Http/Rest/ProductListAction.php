<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Product\Application\Query\ProductList\ProductListQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Products")
 * @Route("/products", methods={"GET"})
 */
class ProductListAction implements ActionInterface
{
    public function __invoke(ProductListRequest $request, CommandBus $bus): ProductListResponse
    {
        $result = $bus->handle(new ProductListQuery(
            $request->getFilter(),
            $request->getRange(),
            $request->getSort()
        ));

        return new ProductListResponse($result);
    }
}
