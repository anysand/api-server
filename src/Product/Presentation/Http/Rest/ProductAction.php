<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Product\Application\Query\Product\ProductQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Product")
 *
 * @Route("/products/{id}", methods={"GET"})
 *
 * @OA\Response(
 *     response=200,
 *     description="Get Product"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 */
class ProductAction implements ActionInterface
{
    public function __invoke(string $id, CommandBus $bus): JsonResponse
    {
        $command = new ProductQuery($id);

        $productView = $bus->handle($command);

        return new ProductResponse($productView);
    }
}
