<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Product\Domain\View\ProductListViewInterface;
use App\Product\Domain\View\ProductViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductListResponse extends JsonResponse
{
    public function __construct(ProductListViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
            ['Content-Range' => 'posts ' . $view->getStartRange() . '-' . $view->getEndRange() . '/' . $view->getTotalCount()]
        );
    }

    private function makePayload(ProductListViewInterface $productListView): array
    {
        return array_map(static function (ProductViewInterface $productView) {
            return [
                'id' => $productView->getId(),
                'name' => $productView->getName(),
            ];
        }, $productListView->getElements());
    }
}
