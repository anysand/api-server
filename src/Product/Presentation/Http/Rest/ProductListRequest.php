<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Request\RequestInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProductListRequest implements RequestInterface
{
    /**
     * @Assert\Type("array")
     * @Assert\Collection(
     *     fields = {
     *         "name" = @Assert\Type("string")
     *     },
     *     allowMissingFields = true,
     *     allowExtraFields = false
     * )
     *
     * @var array
     */
    private array $filter = [];

    /**
     * @Assert\Type("array")
     * @Assert\Collection(
     *     fields = {
     *         "0" = @Assert\Type("integer"),
     *         "1" = @Assert\Type("integer")
     *     },
     *     allowMissingFields = true,
     *     allowExtraFields = false
     * )
     *
     * @var array
     */
    private array $range = [];

    /**
     * @Assert\Type("array")
     * @Assert\Collection(
     *     fields = {
     *         "0" = @Assert\Type("string"),
     *         "1" = @Assert\Type("string")
     *     },
     *     allowMissingFields = true,
     *     allowExtraFields = false
     * )
     *
     * @var array
     */
    private array $sort = [];


    public function getFilter(): array
    {
        return $this->filter;
    }

    /**
     * @return array
     */
    public function getRange(): array
    {
        return $this->range;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }
}
