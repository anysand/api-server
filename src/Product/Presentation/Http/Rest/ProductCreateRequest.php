<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Request\RequestInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema(
 *      title="Create Product request",
 *      description="Create Product request body data",
 *      type="object",
 * )
 */
class ProductCreateRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank
     * @OA\Property(property="name", type="string", example="some name")
     */
    private string $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
