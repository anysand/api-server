<?php

declare(strict_types=1);

namespace App\Product\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Product\Application\Command\DeleteProductCommand;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Product")
 *
 * @Route("/products/{id}", methods={"DELETE"})
 *
 * @OA\Response(
 *     response=204,
 *     description="Task updated successfully"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 */
class ProductDeleteAction implements ActionInterface
{
    public function __invoke(string $id, CommandBus $bus): JsonResponse
    {
        $command = new DeleteProductCommand($id);

        $bus->handle($command);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
