<?php

declare(strict_types=1);

namespace App\Product\Infrastructure\Persistence\Doctrine\Repository;

use App\Common\Domain\Exception\ConflictEntityException;
use App\Common\Domain\Exception\EntityNotFoundException;
use App\Product\Domain\Product;
use App\Product\Domain\Repository\ProductRepositoryInterface;
use App\Product\Domain\ValueObject\ProductId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function store(Product $product): void
    {
        try {
            $this->_em->persist($product);
            $this->_em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictEntityException('Product already exists');
        }
    }

    public function getById(ProductId $id): Product
    {
        /** @var Product|null $product */
        $product = $this->find($id);

        if (null === $product) {
            throw new EntityNotFoundException('Product not found');
        }

        return $product;
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Product $product): void
    {
        $this->_em->remove($product);
        $this->_em->flush();
    }
}
