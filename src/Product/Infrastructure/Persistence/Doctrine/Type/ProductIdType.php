<?php

declare(strict_types=1);

namespace App\Product\Infrastructure\Persistence\Doctrine\Type;

use App\Common\Infrastructure\Persistence\Doctrine\Type\UuidType;
use App\Product\Domain\ValueObject\ProductId;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class ProductIdType extends UuidType
{
    public const NAME = 'product_id';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new ProductId((string)parent::convertToPHPValue($value, $platform));
    }
}
