<?php

declare(strict_types=1);

namespace App\Product\Infrastructure\Query\Doctrine\View;

use App\Common\Infrastructure\ListView;
use App\Product\Domain\View\ProductListViewInterface;

class ProductListView extends ListView implements ProductListViewInterface
{
    protected function getElementType(): string
    {
        return ProductView::class;
    }
}
