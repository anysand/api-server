<?php

declare(strict_types=1);

namespace App\Product\Infrastructure\Query\Doctrine\View;

use App\Product\Domain\View\ProductViewInterface;

class ProductView implements ProductViewInterface
{
    private string $id;

    private string $name;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
