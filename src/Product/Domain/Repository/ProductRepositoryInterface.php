<?php

declare(strict_types=1);

namespace App\Product\Domain\Repository;

use App\Product\Domain\Product;
use App\Product\Domain\ValueObject\ProductId;

interface ProductRepositoryInterface
{
    public function store(Product $Product): void;

    public function getById(ProductId $id): Product;

    public function remove(Product $product): void;
}
