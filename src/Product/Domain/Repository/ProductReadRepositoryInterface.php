<?php

declare(strict_types=1);

namespace App\Product\Domain\Repository;

use App\Product\Domain\View\ProductListViewInterface;
use App\Product\Domain\View\ProductViewInterface;

interface ProductReadRepositoryInterface
{
    public function oneByCriteria(array $criteria): ProductViewInterface;

    public function findAllByCriteria(array $criteria, array $range, array $sort): ProductListViewInterface;
}
