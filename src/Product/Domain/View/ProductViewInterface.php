<?php

declare(strict_types=1);

namespace App\Product\Domain\View;

interface ProductViewInterface
{
    public function getId(): string;

    public function getName(): string;
}
