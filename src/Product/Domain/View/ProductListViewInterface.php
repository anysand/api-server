<?php

declare(strict_types=1);

namespace App\Product\Domain\View;

use App\Common\Domain\View\ListViewInterface;

interface ProductListViewInterface extends ListViewInterface
{
}
