<?php

declare(strict_types=1);

namespace App\Product\Domain\ValueObject;

use App\Common\Domain\ValueObject\AggregateRootId;

final class ProductId extends AggregateRootId
{
}
