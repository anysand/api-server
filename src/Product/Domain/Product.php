<?php

declare(strict_types=1);

namespace App\Product\Domain;

use App\Common\Domain\Event\RaiseEventsInterface;
use App\Common\Domain\Event\RaiseEventsTrait;
use App\Product\Domain\ValueObject\Credential;
use App\Product\Domain\ValueObject\ProductId;
use App\Product\Domain\ValueObject\Name;

class Product implements RaiseEventsInterface
{
    use RaiseEventsTrait;

    private ProductId $id;
    private string $name;

    private function __construct()
    {
//        $this->raise(new ProductWasCreated($this));
    }

    public static function create(ProductId $id, string $name): self
    {
        $self = new self();
        $self->id = $id;
        $self->name = $name;

        return $self;
    }

    public function getId(): ProductId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
