<?php

declare(strict_types=1);

namespace App\User\Domain\View;

interface UserViewInterface
{
    public function getId(): string;

    public function getEmail(): string;
}
