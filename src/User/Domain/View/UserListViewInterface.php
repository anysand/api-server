<?php

declare(strict_types=1);

namespace App\User\Domain\View;

use App\Common\Domain\View\ListViewInterface;

interface UserListViewInterface extends ListViewInterface
{
}
