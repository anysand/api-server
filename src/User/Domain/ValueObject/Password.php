<?php

declare(strict_types=1);

namespace App\User\Domain\ValueObject;

final class Password
{
    protected string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function equals(self $self): bool
    {
        return $this->value === $self->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
