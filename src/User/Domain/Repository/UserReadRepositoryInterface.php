<?php

declare(strict_types=1);

namespace App\User\Domain\Repository;

use App\User\Domain\View\UserListViewInterface;
use App\User\Domain\View\UserViewInterface;

interface UserReadRepositoryInterface
{
    public function oneByCriteria(array $criteria): UserViewInterface;

    public function findAllByCriteria(array $criteria, array $range, array $sort): UserListViewInterface;
}
