<?php

declare(strict_types=1);

namespace App\User\Domain\Repository;

use App\User\Domain\User;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\UserId;

interface UserRepositoryInterface
{
    public function store(User $User): void;

    public function getById(UserId $id): User;

    public function findOneByEmail(Email $email): ?User;

    public function remove(User $User): void;
}
