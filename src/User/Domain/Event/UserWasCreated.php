<?php

declare(strict_types=1);

namespace App\User\Domain\Event;

use App\Common\Domain\Event\Event;
use App\User\Domain\User;

class UserWasCreated extends Event
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }
}
