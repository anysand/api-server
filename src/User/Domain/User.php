<?php

declare(strict_types=1);

namespace App\User\Domain;

use App\Common\Domain\Event\RaiseEventsInterface;
use App\Common\Domain\Event\RaiseEventsTrait;
use App\User\Domain\Event\UserWasCreated;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\Password;
use App\User\Domain\ValueObject\UserId;

class User implements RaiseEventsInterface
{
    use RaiseEventsTrait;

    private UserId $id;
    private Email $email;
    private Password $password;

    private function __construct()
    {
        $this->raise(new UserWasCreated($this));
    }

    public static function create(UserId $id, Email $email, Password $password): self
    {
        $self = new self();
        $self->id = $id;
        $self->email = $email;
        $self->password = $password;

        return $self;
    }

    public function changeEmail(Email $email): void
    {
        $this->email = $email;
    }

    /**
     * @return UserId
     */
    public function getId(): UserId
    {
        return $this->id;
    }

    /**
     * @return Email
     */
    public function getEmail(): Email
    {
        return $this->email;
    }

    /**
     * @return Password
     */
    public function getPassword(): Password
    {
        return $this->password;
    }
}
