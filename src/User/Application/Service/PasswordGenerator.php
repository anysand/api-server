<?php

namespace App\User\Application\Service;

use Exception;

class PasswordGenerator
{
    /**
     * @throws Exception
     */
    public function generate(): string
    {
        return random_bytes(10);
    }
}