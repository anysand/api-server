<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\User\Domain\Repository\UserRepositoryInterface;
use App\User\Domain\User;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\Password;
use App\User\Domain\ValueObject\UserId;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class DeleteUserHandler implements CommandHandlerInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(DeleteUserCommand $command): void
    {
        $user = $this->userRepository->getById(new UserId($command->getId()));

        $this->userRepository->remove($user);
    }
}
