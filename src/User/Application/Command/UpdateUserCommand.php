<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class UpdateUserCommand
{
    /**
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private string $id;

    /**
     * @Assert\Email
     * @Assert\NotBlank
     */
    private string $email;

    public function __construct(string $id, string $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
