<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class CreateUserCommand
{
    /**
     * @Assert\Email
     * @Assert\NotBlank
     */
    private string $email;

    /**
     * @Assert\Type("string")
     */
    private ?string $password;

    public function __construct(string $email, ?string $password = null)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
}
