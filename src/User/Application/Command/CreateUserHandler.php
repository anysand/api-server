<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\Common\Infrastructure\Subscriber\DomainEventsCollector;
use App\User\Application\Service\PasswordGenerator;
use App\User\Domain\Repository\UserRepositoryInterface;
use App\User\Domain\User;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\Password;
use App\User\Domain\ValueObject\UserId;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class CreateUserHandler implements CommandHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private PasswordHasherFactoryInterface $hasherFactory;
    private DomainEventsCollector $eventsCollector;
    private PasswordGenerator $passwordGenerator;

    public function __construct(
        UserRepositoryInterface $userRepository,
        PasswordHasherFactoryInterface $hasherFactory,
        DomainEventsCollector $eventsCollector,
        PasswordGenerator $passwordGenerator)
    {
        $this->userRepository = $userRepository;
        $this->hasherFactory = $hasherFactory;
        $this->eventsCollector = $eventsCollector;
        $this->passwordGenerator = $passwordGenerator;
    }

    public function handle(CreateUserCommand $command): User
    {
        $password = $command->getPassword() ?? $this->passwordGenerator->generate();
        $hashedPassword = $this->hasherFactory->getPasswordHasher(User::class)->hash($password);

        $user = User::create(
            new UserId(),
            new Email($command->getEmail()),
            new Password($hashedPassword)
        );

        $this->userRepository->store($user);
        $this->eventsCollector->dispatchCollectedEvents();

        return $user;
    }
}
