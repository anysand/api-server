<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class DeleteUserCommand
{
    /**
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
