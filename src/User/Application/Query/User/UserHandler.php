<?php

declare(strict_types=1);

namespace App\User\Application\Query\User;

use App\Common\Application\QueryHandlerInterface;
use App\User\Domain\Repository\UserReadRepositoryInterface;
use App\User\Domain\View\UserViewInterface;

class UserHandler implements QueryHandlerInterface
{
    private UserReadRepositoryInterface $repository;

    public function __construct(UserReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(UserQuery $query): UserViewInterface
    {
        return $this->repository->oneByCriteria(['id' => $query->getId()]);
    }
}
