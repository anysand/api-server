<?php

declare(strict_types=1);

namespace App\User\Application\Query\UserList;

use App\Common\Application\QueryHandlerInterface;
use App\User\Domain\Repository\UserReadRepositoryInterface;

class UserListHandler implements QueryHandlerInterface
{
    private UserReadRepositoryInterface $readRepository;

    public function __construct(UserReadRepositoryInterface $readRepository)
    {
        $this->readRepository = $readRepository;
    }

    public function handle(UserListQuery $query)
    {
        return $this->readRepository->findAllByCriteria($query->getFilter(), $query->getRange(), $query->getSort());
    }
}
