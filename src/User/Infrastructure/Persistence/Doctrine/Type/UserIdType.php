<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence\Doctrine\Type;

use App\Common\Infrastructure\Persistence\Doctrine\Type\UuidType;
use App\User\Domain\ValueObject\UserId;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class UserIdType extends UuidType
{
    public const NAME = 'user_id';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new UserId((string) parent::convertToPHPValue($value, $platform));
    }
}
