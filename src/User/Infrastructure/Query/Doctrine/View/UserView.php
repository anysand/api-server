<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Query\Doctrine\View;

use App\User\Domain\View\UserViewInterface;

class UserView implements UserViewInterface
{
    private string $id;

    private string $email;

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
