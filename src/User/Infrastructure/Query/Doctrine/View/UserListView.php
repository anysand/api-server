<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Query\Doctrine\View;

use App\Common\Infrastructure\ListView;
use App\User\Domain\View\UserListViewInterface;

class UserListView extends ListView implements UserListViewInterface
{
    protected function getElementType(): string
    {
        return UserView::class;
    }
}
