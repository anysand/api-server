<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Query\Doctrine\Repository;

use App\Common\Domain\Exception\EntityNotFoundException;
use App\User\Domain\Repository\UserReadRepositoryInterface;
use App\User\Domain\View\UserListViewInterface;
use App\User\Domain\View\UserViewInterface;
use App\User\Infrastructure\Query\Doctrine\View\UserListView;
use App\User\Infrastructure\Query\Doctrine\View\UserView;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class FormReadModelRepository.
 */
class UserReadRepository implements UserReadRepositoryInterface
{
    private const TABLE_NAME = '"user"';

    private const COLUMN_MAPPING = [
        'email' => 'email_value',
        'id' => 'id',
    ];

    private Connection $connection;

    private DenormalizerInterface $normalizer;

    public function __construct(Connection $connection, DenormalizerInterface $normalizer)
    {
        $this->connection = $connection;
        $this->normalizer = $normalizer;
    }

    /**
     * @throws Exception
     */
    public function oneByCriteria(array $criteria): UserViewInterface
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('id, email_value as email')
            ->from(self::TABLE_NAME);

        $this->applyFilter($qb, $criteria);

        $result = $qb->fetchAssociative();

        if (false === $result) {
            throw new EntityNotFoundException('User not found');
        }

        return $this->normalizer->denormalize($result, UserView::class);
    }

    public function findAllByCriteria(array $criteria, array $range, array $sort): UserListViewInterface
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('count(id)')
            ->from(self::TABLE_NAME);

        $this->applyFilter($qb, $criteria);
        $count = $qb->fetchOne();

        $qb->select('id, email_value as email');
        $this->applyRange($qb, $range);
        $this->applySort($qb, $sort);

        $result = $qb->fetchAllAssociative();

        /** @var UserView[] $objects */
        $objects = $this->normalizer->denormalize($result, UserView::class.'[]');

        return new UserListView(
            $objects,
            (int) ($range[0] ?? 0),
            (int) ($range[0] ?? 0) + count($objects),
            (int) $count
        );
    }

    /**
     * @throws \Exception
     */
    private function applyFilter(QueryBuilder $qb, array $params): void
    {
        foreach ($params as $column => $value) {
            $qb->andWhere($this->getMappedField($column).'= :'.$column)
                ->setParameter($column, $value);
        }
    }

    /**
     * @throws \Exception
     */
    private function applyRange(QueryBuilder $qb, array $params): void
    {
        $qb->setFirstResult($params[0] ?? 0);
        $qb->setMaxResults($params[1] - $params[0]);
    }

    /**
     * @throws \Exception
     */
    private function applySort(QueryBuilder $queryBuilder, array $params): void
    {
        if (array_key_exists(0, $params)) {
            $queryBuilder->orderBy($this->getMappedField($params[0]), $params[1] ?? null);
        }
    }

    /**
     * @param string $column
     *
     * @return string
     *
     * @throws \Exception
     */
    private function getMappedField(string $column): string
    {
        if (!isset(self::COLUMN_MAPPING[$column])) {
            throw new \Exception('Invalid filter data');
        }

        return self::COLUMN_MAPPING[$column];
    }
}
