<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Request\RequestInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema(
 *      title="Create User request",
 *      description="Create User request body data",
 *      type="object",
 * )
 */
class UserCreateRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank
     * @OA\Property(property="email", type="email", example="Email")
     */
    private string $email = '';

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 6,
     *      max = 50,
     *      minMessage = "Password must be at least {{ limit }} characters long",
     *      maxMessage = "Password cannot be longer than {{ limit }} characters"
     * )
     * @OA\Property(property="password", type="string")
     */
    private string $password = '';

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
