<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\User\Application\Query\User\UserQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="User")
 *
 * @Route("/users/{id}", methods={"GET"})
 *
 * @OA\Response(
 *     response=200,
 *     description="Get User"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 */
class UserAction implements ActionInterface
{
    public function __invoke(string $id, CommandBus $bus): JsonResponse
    {
        $command = new UserQuery($id);

        $userView = $bus->handle($command);

        return new UserResponse($userView);
    }
}
