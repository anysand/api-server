<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\User\Domain\View\UserListViewInterface;
use App\User\Domain\View\UserViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserListResponse extends JsonResponse
{
    public function __construct(UserListViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
            ['Content-Range' => 'posts ' . $view->getStartRange() . '-' . $view->getEndRange() . '/' . $view->getTotalCount()]
        );
    }

    private function makePayload(UserListViewInterface $userListView): array
    {
        return array_map(static function (UserViewInterface $userView) {
            return [
                'id' => $userView->getId(),
                'email' => $userView->getEmail(),
            ];
        }, $userListView->getElements());
    }
}
