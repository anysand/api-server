<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\User\Application\Command\CreateUserCommand;
use App\User\Domain\User;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="User")
 *
 * @Route("/users", methods={"POST"})
 *
 * @OA\Response(
 *     response=201,
 *     description="Task created successfully"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 * @OA\Response(
 *     response=409,
 *     description="Conflict"
 * )
 */
class UserCreateAction implements ActionInterface
{
    public function __invoke(UserCreateRequest $request, CommandBus $bus): JsonResponse
    {
        $command = new CreateUserCommand($request->getEmail());

        /** @var User $user */
        $user = $bus->handle($command);

        return new JsonResponse([
            'uuid' => (string) $user->getId()
        ], Response::HTTP_CREATED);
    }
}
