<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\User\Application\Command\UpdateUserCommand;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="User")
 *
 * @Route("/users/{id}", methods={"PUT"})
 *
 * @OA\Response(
 *     response=204,
 *     description="Task updated successfully"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 */
class UserUpdateAction implements ActionInterface
{
    public function __invoke(string $id, UserUpdateRequest $request, CommandBus $bus): JsonResponse
    {
        $command = new UpdateUserCommand(
            $id,
            $request->getEmail()
        );

        $bus->handle($command);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
