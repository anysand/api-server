<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\User\Application\Query\UserList\UserListQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Users")
 * @Route("/users", methods={"GET"})
 */
class UserListAction implements ActionInterface
{
    public function __invoke(UserListRequest $request, CommandBus $bus)
    {
        $result = $bus->handle(new UserListQuery(
            $request->getFilter(),
            $request->getRange(),
            $request->getSort()
        ));

        return new UserListResponse($result);
    }
}
