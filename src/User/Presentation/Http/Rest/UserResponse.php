<?php

declare(strict_types=1);

namespace App\User\Presentation\Http\Rest;

use App\User\Domain\View\UserViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserResponse extends JsonResponse
{
    public function __construct(UserViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
        );
    }

    private function makePayload(UserViewInterface $view): array
    {
        return [
            'id' => $view->getId(),
            'email' => $view->getEmail(),
        ];
    }
}
