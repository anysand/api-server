<?php

declare(strict_types=1);

namespace App\User\Presentation\Cli\Command;

use App\User\Application\Command\CreateUserCommand as AppCreateUserCommand;
use Exception;
use League\Tactician\CommandBus;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:user:create';

    private ValidatorInterface $validator;
    private CommandBus $bus;

    public function __construct(ValidatorInterface $validator, CommandBus $bus)
    {
        parent::__construct();
        $this->validator = $validator;
        $this->bus = $bus;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Create new user')
            ->setHelp(
                'This command create new user.'
            );
    }

    /**
     * Executes the current command.
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $io = new SymfonyStyle($input, $output);

        $emailQuestion = $this->createEmailQuestion();
        $email = $io->askQuestion($emailQuestion);

        $passwordQuestion = $this->createPasswordQuestion();
        $password = $io->askQuestion($passwordQuestion);

        $command = new AppCreateUserCommand(
            $email,
            $password
        );

        $this->bus->handle($command);

        return Command::SUCCESS;
    }

    /**
     * Create the email question to ask the user for the email.
     */
    private function createEmailQuestion(): Question
    {
        $passwordQuestion = new Question('Type in your email');

        return $passwordQuestion
            ->setValidator(function ($value) {
                $errors = $this->validator->validatePropertyValue(AppCreateUserCommand::class, 'email', $value);

                if (count($errors) > 0) {
                    throw new RuntimeException($errors->get(0)->getMessage());
                }

                return $value;
            })
            ->setMaxAttempts(20);
    }

    /**
     * Create the password question to ask the user for the password to be hashed.
     */
    private function createPasswordQuestion(): Question
    {
        $passwordQuestion = new Question('Type in your password to be hashed');

        return $passwordQuestion
            ->setValidator(function ($value) {
                $errors = $this->validator->validatePropertyValue(AppCreateUserCommand::class, 'password', $value);

                if (count($errors) > 0) {
                    throw new RuntimeException($errors->get(0)->getMessage());
                }

                return $value;
            })
            ->setHidden(true)
            ->setMaxAttempts(20);
    }
}
