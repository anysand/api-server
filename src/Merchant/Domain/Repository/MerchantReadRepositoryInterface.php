<?php

declare(strict_types=1);

namespace App\Merchant\Domain\Repository;

use App\Merchant\Domain\View\MerchantListViewInterface;
use App\Merchant\Domain\View\MerchantViewInterface;

interface MerchantReadRepositoryInterface
{
    public function oneByCriteria(array $criteria): MerchantViewInterface;

    public function findAllByCriteria(array $criteria, array $range, array $sort): MerchantListViewInterface;
}
