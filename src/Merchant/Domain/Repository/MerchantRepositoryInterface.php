<?php

declare(strict_types=1);

namespace App\Merchant\Domain\Repository;

use App\Merchant\Domain\Merchant;
use App\Merchant\Domain\ValueObject\MerchantId;

interface MerchantRepositoryInterface
{
    public function store(Merchant $Merchant): void;

    public function getById(MerchantId $id): Merchant;

    public function remove(Merchant $merchant): void;
}
