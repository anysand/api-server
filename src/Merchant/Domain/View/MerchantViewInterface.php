<?php

declare(strict_types=1);

namespace App\Merchant\Domain\View;

interface MerchantViewInterface
{
    public function getId(): string;

    public function getName(): string;
}
