<?php

declare(strict_types=1);

namespace App\Merchant\Domain\View;

use App\Common\Domain\View\ListViewInterface;

interface MerchantListViewInterface extends ListViewInterface
{
}
