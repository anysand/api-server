<?php

declare(strict_types=1);

namespace App\Merchant\Domain;

use App\Common\Domain\Event\RaiseEventsInterface;
use App\Common\Domain\Event\RaiseEventsTrait;
use App\Merchant\Domain\ValueObject\Credential;
use App\Merchant\Domain\ValueObject\MerchantId;
use App\Merchant\Domain\ValueObject\Name;

class Merchant implements RaiseEventsInterface
{
    use RaiseEventsTrait;

    private MerchantId $id;
    private string $name;

    private function __construct()
    {
//        $this->raise(new MerchantWasCreated($this));
    }

    public static function create(MerchantId $id, string $name): self
    {
        $self = new self();
        $self->id = $id;
        $self->name = $name;

        return $self;
    }

    public function getId(): MerchantId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
