<?php

declare(strict_types=1);

namespace App\Merchant\Domain\ValueObject;

use App\Common\Domain\ValueObject\AggregateRootId;

final class MerchantId extends AggregateRootId
{
}
