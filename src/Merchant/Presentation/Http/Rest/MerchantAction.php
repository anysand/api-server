<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Merchant\Application\Query\Merchant\MerchantQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Merchant")
 *
 * @Route("/merchants/{id}", methods={"GET"})
 *
 * @OA\Response(
 *     response=200,
 *     description="Get Merchant"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 */
class MerchantAction implements ActionInterface
{
    public function __invoke(string $id, CommandBus $bus): JsonResponse
    {
        $command = new MerchantQuery($id);

        $merchantView = $bus->handle($command);

        return new MerchantResponse($merchantView);
    }
}
