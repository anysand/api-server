<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Merchant\Application\Query\MerchantList\MerchantListQuery;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Merchants")
 * @Route("/merchants", methods={"GET"})
 */
class MerchantListAction implements ActionInterface
{
    public function __invoke(MerchantListRequest $request, CommandBus $bus): MerchantListResponse
    {
        $result = $bus->handle(new MerchantListQuery(
            $request->getFilter(),
            $request->getRange(),
            $request->getSort()
        ));

        return new MerchantListResponse($result);
    }
}
