<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Http\Rest;

use App\Merchant\Domain\View\MerchantListViewInterface;
use App\Merchant\Domain\View\MerchantViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class MerchantListResponse extends JsonResponse
{
    public function __construct(MerchantListViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
            ['Content-Range' => 'posts ' . $view->getStartRange() . '-' . $view->getEndRange() . '/' . $view->getTotalCount()]
        );
    }

    private function makePayload(MerchantListViewInterface $merchantListView): array
    {
        return array_map(static function (MerchantViewInterface $merchantView) {
            return [
                'id' => $merchantView->getId(),
                'name' => $merchantView->getName(),
            ];
        }, $merchantListView->getElements());
    }
}
