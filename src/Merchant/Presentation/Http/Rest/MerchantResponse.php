<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Http\Rest;

use App\Merchant\Domain\View\MerchantViewInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class MerchantResponse extends JsonResponse
{
    public function __construct(MerchantViewInterface $view)
    {
        parent::__construct(
            $this->makePayload($view),
            200,
        );
    }

    private function makePayload(MerchantViewInterface $view): array
    {
        return [
            'id' => $view->getId(),
            'name' => $view->getName(),
        ];
    }
}
