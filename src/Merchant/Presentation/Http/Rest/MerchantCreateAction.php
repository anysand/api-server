<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Http\Rest;

use App\Common\Presentation\Http\Rest\Action\ActionInterface;
use App\Merchant\Application\Command\CreateMerchantCommand;
use App\Merchant\Domain\Merchant;
use League\Tactician\CommandBus;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Merchant")
 *
 * @Route("/merchants", methods={"POST"})
 *
 * @OA\Response(
 *     response=201,
 *     description="Task created successfully"
 * )
 * @OA\Response(
 *     response=400,
 *     description="Bad request"
 * )
 * @OA\Response(
 *     response=409,
 *     description="Conflict"
 * )
 */
class MerchantCreateAction implements ActionInterface
{
    public function __invoke(MerchantCreateRequest $request, CommandBus $bus): JsonResponse
    {
        $command = new CreateMerchantCommand($request->getName());

        /** @var Merchant $merchant */
        $merchant = $bus->handle($command);

        return new JsonResponse([
            'uuid' => (string) $merchant->getId()
        ], Response::HTTP_CREATED);
    }
}
