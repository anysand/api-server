<?php

declare(strict_types=1);

namespace App\Merchant\Presentation\Cli\Command;

use App\Merchant\Application\Command\CreateMerchantCommand as AppCreateMerchantCommand;
use Exception;
use League\Tactician\CommandBus;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateMerchantCommand extends Command
{
    protected static $defaultName = 'app:merchant:create';

    private ValidatorInterface $validator;
    private CommandBus $bus;

    public function __construct(ValidatorInterface $validator, CommandBus $bus)
    {
        parent::__construct();
        $this->validator = $validator;
        $this->bus = $bus;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Create new merchant')
            ->setHelp(
                'This command create new merchant.'
            );
    }

    /**
     * Executes the current command.
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $io = new SymfonyStyle($input, $output);

        $nameQuestion = $this->createEmailQuestion();
        $name = $io->askQuestion($nameQuestion);

        $command = new AppCreateMerchantCommand(
            $name
        );

        $this->bus->handle($command);

        return Command::SUCCESS;
    }

    /**
     * Create the name question to ask the merchant for the name.
     */
    private function createEmailQuestion(): Question
    {
        $passwordQuestion = new Question('Type in merchant name');

        return $passwordQuestion
            ->setValidator(function ($value) {
                $errors = $this->validator->validatePropertyValue(AppCreateMerchantCommand::class, 'name', $value);

                if (count($errors) > 0) {
                    throw new RuntimeException($errors->get(0)->getMessage());
                }

                return $value;
            })
            ->setMaxAttempts(20);
    }
}
