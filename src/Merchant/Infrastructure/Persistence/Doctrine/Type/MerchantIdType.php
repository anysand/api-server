<?php

declare(strict_types=1);

namespace App\Merchant\Infrastructure\Persistence\Doctrine\Type;

use App\Common\Infrastructure\Persistence\Doctrine\Type\UuidType;
use App\Merchant\Domain\ValueObject\MerchantId;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class MerchantIdType extends UuidType
{
    public const NAME = 'merchant_id';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new MerchantId((string)parent::convertToPHPValue($value, $platform));
    }
}
