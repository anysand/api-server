<?php

declare(strict_types=1);

namespace App\Merchant\Infrastructure\Persistence\Doctrine\Repository;

use App\Common\Domain\Exception\ConflictEntityException;
use App\Common\Domain\Exception\EntityNotFoundException;
use App\Merchant\Domain\Merchant;
use App\Merchant\Domain\Repository\MerchantRepositoryInterface;
use App\Merchant\Domain\ValueObject\MerchantId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;

class MerchantRepository extends ServiceEntityRepository implements MerchantRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Merchant::class);
    }

    /**
     * @param Merchant $merchant
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function store(Merchant $merchant): void
    {
        try {
            $this->_em->persist($merchant);
            $this->_em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictEntityException('Merchant already exists');
        }
    }

    public function getById(MerchantId $id): Merchant
    {
        /** @var Merchant|null $merchant */
        $merchant = $this->find($id);

        if (null === $merchant) {
            throw new EntityNotFoundException('Merchant not found');
        }

        return $merchant;
    }

    /**
     * @param Merchant $merchant
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Merchant $merchant): void
    {
        $this->_em->remove($merchant);
        $this->_em->flush();
    }
}
