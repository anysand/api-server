<?php

declare(strict_types=1);

namespace App\Merchant\Infrastructure\Query\Doctrine\View;

use App\Common\Infrastructure\ListView;
use App\Merchant\Domain\View\MerchantListViewInterface;

class MerchantListView extends ListView implements MerchantListViewInterface
{
    protected function getElementType(): string
    {
        return MerchantView::class;
    }
}
