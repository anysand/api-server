<?php

declare(strict_types=1);

namespace App\Merchant\Infrastructure\Query\Doctrine\View;

use App\Merchant\Domain\View\MerchantViewInterface;

class MerchantView implements MerchantViewInterface
{
    private string $id;

    private string $name;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
