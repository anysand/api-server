<?php

declare(strict_types=1);

namespace App\Merchant\Application\Query\Merchant;

use App\Common\Application\QueryHandlerInterface;
use App\Merchant\Domain\Repository\MerchantReadRepositoryInterface;
use App\Merchant\Domain\View\MerchantViewInterface;

class MerchantHandler implements QueryHandlerInterface
{
    private MerchantReadRepositoryInterface $repository;

    public function __construct(MerchantReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(MerchantQuery $query): MerchantViewInterface
    {
        return $this->repository->oneByCriteria(['id' => $query->getId()]);
    }
}
