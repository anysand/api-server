<?php

declare(strict_types=1);

namespace App\Merchant\Application\Query\Merchant;

use Symfony\Component\Validator\Constraints as Assert;

class MerchantQuery
{
    /**
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
