<?php

declare(strict_types=1);

namespace App\Merchant\Application\Query\MerchantList;

use App\Common\Application\QueryHandlerInterface;
use App\Merchant\Domain\Repository\MerchantReadRepositoryInterface;
use App\Merchant\Domain\View\MerchantListViewInterface;

class MerchantListHandler implements QueryHandlerInterface
{
    private MerchantReadRepositoryInterface $readRepository;

    public function __construct(MerchantReadRepositoryInterface $readRepository)
    {
        $this->readRepository = $readRepository;
    }

    public function handle(MerchantListQuery $query): MerchantListViewInterface
    {
        return $this->readRepository->findAllByCriteria($query->getFilter(), $query->getRange(), $query->getSort());
    }
}
