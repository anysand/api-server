<?php

declare(strict_types=1);

namespace App\Merchant\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\Common\Infrastructure\Subscriber\DomainEventsCollector;
use App\Merchant\Domain\Merchant;
use App\Merchant\Domain\Repository\MerchantRepositoryInterface;
use App\Merchant\Domain\ValueObject\MerchantId;

class CreateMerchantHandler implements CommandHandlerInterface
{
    private MerchantRepositoryInterface $merchantRepository;
    private DomainEventsCollector $eventsCollector;

    public function __construct(
        MerchantRepositoryInterface $merchantRepository,
        DomainEventsCollector $eventsCollector)
    {
        $this->merchantRepository = $merchantRepository;
        $this->eventsCollector = $eventsCollector;
    }

    public function handle(CreateMerchantCommand $command): Merchant
    {
        $merchant = Merchant::create(
            new MerchantId(),
            $command->getName());

        $this->merchantRepository->store($merchant);
        $this->eventsCollector->dispatchCollectedEvents();

        return $merchant;
    }
}
