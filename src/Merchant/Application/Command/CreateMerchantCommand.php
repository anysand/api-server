<?php

declare(strict_types=1);

namespace App\Merchant\Application\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class CreateMerchantCommand
{
    /**
     * @Assert\NotBlank
     */
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
