<?php

declare(strict_types=1);

namespace App\Merchant\Application\Command;

use App\Common\Application\CommandHandlerInterface;
use App\Merchant\Domain\Repository\MerchantRepositoryInterface;
use App\Merchant\Domain\ValueObject\Email;
use App\Merchant\Domain\ValueObject\MerchantId;
use App\Merchant\Domain\ValueObject\Password;

class DeleteMerchantHandler implements CommandHandlerInterface
{
    private MerchantRepositoryInterface $merchantRepository;

    public function __construct(MerchantRepositoryInterface $merchantRepository)
    {
        $this->merchantRepository = $merchantRepository;
    }

    public function handle(DeleteMerchantCommand $command): void
    {
        $merchant = $this->merchantRepository->getById(new MerchantId($command->getId()));

        $this->merchantRepository->remove($merchant);
    }
}
