-include ./.env
export

include ./.make/test.mk

ARGS = $(filter-out $@,$(MAKECMDGOALS))

build-images: ## build
	docker build .docker/php/fpm/ -t $(REGISTRY_PATH)/app/base
	docker build .docker/php/composer/ -t $(REGISTRY_PATH)/app/composer --build-arg BASE_IMAGE=$(REGISTRY_PATH)/app/base
	docker build .docker/php/dev/ -t $(REGISTRY_PATH)/app/dev --build-arg BASE_IMAGE=$(REGISTRY_PATH)/app/composer
	docker build .docker/rabbitmq/ -t $(REGISTRY_PATH)/rabbitmq
	docker build .docker/proxy/ -t $(REGISTRY_PATH)/proxy
	docker build .docker/code-quality/ -t $(REGISTRY_PATH)/code-quality

code-analyse-deptrac: ## analyse code with deptrac
	docker run --rm -ti -v $(shell pwd):/code $(REGISTRY_PATH)/code-quality deptrac --no-cache analyse depfile.domain.yaml
	docker run --rm -ti -v $(shell pwd):/code $(REGISTRY_PATH)/code-quality deptrac --no-cache analyse depfile.layer.yaml

composer-install: ## install composer dependencies
	docker run --rm -t \
		-v $(shell pwd):/var/www/html \
		-v ~/.composer/cache:/.composer_cache \
		$(REGISTRY_PATH)/app/composer composer install --no-scripts

fix-permission: ## fix permission
	sudo chown -R $(shell whoami):$(shell whoami) src config tests
	sudo chown -R $(shell whoami):$(shell whoami) *
	sudo chmod 0777 -R var

clear-var:
	sudo rm -rf var/*

.PHONY: help
help: ## Display this help message
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
