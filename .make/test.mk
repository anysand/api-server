test-up:
	docker-compose -f docker-compose.test.yml -p test up -d
	#docker-compose -f docker-compose.test.yml -p test exec app bash -c '/wait-for -t 600 db:3306 -- echo "DB is ready"'
	docker-compose -f docker-compose.test.yml -p test exec app sh -c 'php bin/console doctrine:migrations:migrate --no-interaction'

test-run:
	docker-compose -f docker-compose.test.yml -p test exec app sh -c './vendor/bin/codecept run tests/ --verbose'

test-down:
	docker-compose -f docker-compose.test.yml -p test down

test-snippets:
	docker-compose -f docker-compose.test.yml -p test exec app sh -c './vendor/bin/codecept gherkin:snippets acceptance'

#test-run-with-coverage:
#	docker-compose -f docker-compose.test.yml -p test exec app bash -c './vendor/bin/codecept run tests/ --coverage-html /opt/coverage-html'

test:
	make test-up
	make test-run || true
	make test-down
